FROM alpine

ENV REMOTE_MIRROR "rsync://thing.com"

RUN apk update
RUN apk add --update python rsync openssh supervisor

ADD rsyncmirror /usr/local/bin/rsyncmirror
ADD supervisord.conf /etc/supervisord.conf

RUN mkdir /data

VOLUME ["/data"]
CMD ["/usr/bin/supervisord"]
